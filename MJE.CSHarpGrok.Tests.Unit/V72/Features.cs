using System;
using NUnit.Framework;

namespace MJE.CSHarpGrok.Tests.Unit.V72
{
    [TestFixture]
    public class Features
    {
        [Test]
        public void Enhancements()
        {
            
            var b2 = new Bar() {Name = "steve"};
            
            V72Foo.Process(b2);

            Console.WriteLine(b2.Name);
            
        }


        [Test]
        public void NonTrailingNamedArguments()
        {
            var s = new V72Foo();

            // because the names are in the required order, bar (2nd arg)
            // doesnt have to be explicitly labelled in the args list.
            
            s.HandleRequest(foo: "sally", "sid", name: "Tez");
        }

        [Test]
        public void LeadingUnderscoreInNumericLiterals()
        {
            // 0b marks the int as binary (read bits r to l)
            const int binaryValue = 0b_0101_0101;
           
            Console.WriteLine(binaryValue.ToString());
        }

        [Test]
        public void PrivateProtectedAccessModifier()
        {
            // ctor accesses a Private Protected modifier in the base class
            var s = new V72FooExtender();
        }
        
    }
}