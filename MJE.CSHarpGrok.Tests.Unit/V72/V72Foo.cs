using System;

namespace MJE.CSHarpGrok.Tests.Unit.V72
{
    public class V72Foo
    {
        private readonly  Bar _bar = new Bar();

        private protected void Fizz()
        {
            Console.WriteLine($" {nameof(this.Fizz)} - I can only be accessed by containing class or derived classes named declared in this assy");
        }
        
        /// <summary>
        /// ref modifier 
        /// </summary>
        /// <returns></returns>
        public ref readonly Bar GetBarByRef()
        {
            return ref _bar;
        }

        /// <summary>
        /// in modifier specifies that the parameter cannot be modified by the method to which it is passed
        /// </summary>
        /// <param name="bar"></param>
        public static void Process(in Bar bar)
        {
            Console.WriteLine(bar);
            
            // This line below wont compile as it was passed using the "in" modifier.
            //bar.Name = "dave";
        }

        public void HandleRequest(string foo, string bar, string name)
        {
            Console.WriteLine($"foo {foo}\nbar {bar}\nname {name}");
        }
    }
}