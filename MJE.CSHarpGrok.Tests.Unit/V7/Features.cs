using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MJE.CSHarpGrok.Tests.Unit.V7
{
    [TestFixture]
    public class Features
    {
        [Test]
        public void OutVariables()
        {
            var s = new V7Foo();

            // "bar" is declared inline using the var keyword
            s.Process("fred", out var bar);

            Assert.NotNull(bar);

            TestContext.Out.WriteLine(bar);
        }

        [Test]
        public void Tuples()
        {
            var s = new V7Foo();

            var vals = s.DtoTuple();

            Assert.NotNull(vals.Alpha);
            Assert.NotNull(vals.Beta);
            
            // Example of deconstructing a tuple
            var (alpha, beta) = s.DtoTuple();

            Assert.NotNull(alpha);
            Assert.NotNull(beta);
        }

        [Test]
        public void Discards()
        {
            var s = new V7Foo();

            // we've no interest in the first two values returned in the tuple
            // so we discard them with the underscore.
            var (_, _, charlie) = s.LargerDtoTuple();
            
            Assert.NotNull(charlie);
                        
            // Not interested in the op variable in method below so we discard it.
            s.Process("sid", out _);

        }

        [Test]
        public void PatternMatching()
        {
            var s = new V7Foo();

            var sum = s.SumOfNumbers(new object[] {1, "1", null, 3});
            
            Assert.AreEqual(4, sum);

        }

        [Test]
        public async Task LocalFunctions()
        {
            var s = new V7Foo();
            
            await s.Process2(bar:"");
            
            await s.Process2(bar:"bar");
        
        }

        [Test]
        public void ExpressionBodiedMembers()
        {
            var s = new V7Foo("fred");
            Assert.AreEqual("fred", s.Name);

            s = new V7Foo();
            Assert.AreEqual(V7Foo.DefaultName, s.Name);
        }

        [Test]
        public void ThrowExpressions()
        {
            var s = new V7Foo();

            Assert.Throws<NullReferenceException>(() => s.Bar = null);
        }

        [Test]
        public async Task GeneralizedAsyncTypes()
        {
            var s = new V7Foo();

            var res = await s.GiveMeAValue();
            
            Assert.NotZero(res);
        }

        [Test]
        public void NumericLiteralSyntax()
        {
            // underscores between characters in a numeric value improve readability
            const int oneMillion = 1_000_000;
            
            Assert.AreEqual(1000000, oneMillion);
        }
    }
}