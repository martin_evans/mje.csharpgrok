using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MJE.CSHarpGrok.Tests.Unit.V7
{
    public class V7Foo
    {
        public const string DefaultName = "DefaultName";
        
        private string _name;
        private string _bar;

        /// <summary>
        /// Demonstrates an expression-bodied member 
        /// </summary>
        /// <param name="name"></param>
        public V7Foo(string name) => this._name = name;
        
        
        public V7Foo() { }

        /// <summary>
        /// Demonstrates an expression-bodied member
        /// </summary>
        public string Name
        {
            get => _name ?? DefaultName;
            set => _name = value ?? DefaultName;
        }

        public string Bar
        {
            get => _bar;
            set => _bar = value ?? throw new NullReferenceException("Cannot be null");
        }

        public void Process(string name, out string o)
        {
            o = $"Hello {name}";
            Console.WriteLine(o);
        }

        /// <summary>
        /// Demonstrates the use of a tuple 
        /// </summary>
        /// <returns></returns>
        public (string Alpha, string Beta) DtoTuple()
        {
            (string Alpha, string Beta) ret = ("a", "b");

            return ret;
        }
        
        public (string Alpha, string Beta, string Charlie) LargerDtoTuple()
        {
            (string Alpha, string Beta, string Charlie) ret = ("a", "b", "c");

            return ret;
        }


        public int SumOfNumbers(IEnumerable<object> objects)
        {
            return  objects.OfType<int>().Sum(x=>x);
        }

        
        /// <summary>
        /// Demonstrates a local function 
        /// </summary>
        /// <param name="bar"></param>
        /// <returns></returns>
        public Task Process2(string bar)
        {
            Console.WriteLine(nameof(Process2));
            
            return bar == string.Empty ? 
                Task.Run(()=> { Console.WriteLine("No value passed"); }) 
                : DoPotentiallyLongRunningWork();
            
            Task DoPotentiallyLongRunningWork()
            {
                Console.WriteLine($"Hello {bar}");
                return Task.FromResult(true);   
            };

        }

        
        /// <summary>
        /// Demonstrate the lightweight task type that returns a value type rather than a ref type
        /// which may be more efficient in certain circumstances. 
        /// </summary>
        /// <returns></returns>
        public async ValueTask<int> GiveMeAValue()
        {
            return await Task.FromResult(1);
        }
    }
}