using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace MJE.CSHarpGrok.Tests.Unit.V9
{
    public record Student(string Forename, string Surname)
    {
        public void Validate()
        {
            if (Forename=="Nigel")
            {
                throw new InvalidDataException("Nigel is now an invalid name");
            }
        }
    }
}