namespace MJE.CSHarpGrok.Tests.Unit.V9
{
    public class Person
    {
        public string Forename { get; init; }
        public string Surname  { get; init; }

        public override string ToString()
        {
            return $"{Forename} {Surname}";
        }
    }
}