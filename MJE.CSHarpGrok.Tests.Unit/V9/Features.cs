using System;
using System.Runtime.CompilerServices;
using NUnit.Framework;

namespace MJE.CSHarpGrok.Tests.Unit.V9
{
    public class Features
    {
        [Test]
        public void InitOnlyProperties()
        {
            var person = new Person { Forename = "martin", Surname = "evans" };

            Console.WriteLine(person);

            // will not compile as the forename may only be set during initialisation

            //person.Forename = "dave";
        }

        [Test]
        public void RecordTypes()
        {
            // Record types are reference type that provides synthesized methods to provide value semantics for equality

            // record types are deemed to be equal when the properties are the same
            var s1 = new Student("Fred", "Blogs");
            var s2 = new Student("Fred", "Blogs");
            Assert.AreEqual(s1, s2);

            // as opposed to classes like below
            var p1 = new Person { Forename = "martin", Surname = "evans" };
            var p2 = new Person { Forename = "martin", Surname = "evans" };
            Assert.AreNotEqual(p1, p2);

            var s3 = s1 with { Forename = "Dave" };

            DoSomethingWithAStudent(s3);

            Console.WriteLine(s1.ToString());
        }

        /// <summary>
        /// Demonstrates that you can pass a record type as a param
        /// </summary>
        private void DoSomethingWithAStudent(Student theStudent)
        {
            Console.WriteLine(theStudent.Forename + "_" + theStudent.Surname);
        }

        [Test]
        public void PatternMatching()
        {
            static bool Matcher(char c) => c is >= 'a' and <= 'z' or >= 'A' and <= 'Z';
            static bool NotEmptyChecker(string c) => c is not "";

            Assert.True(Matcher("c".ToCharArray()[0]));

            Assert.True(NotEmptyChecker("f"));
        }

        [Test]
        public void OmittingTypeDuringInstantiation()
        {
            Person p = new() { Forename = "fred", Surname = "smith" };

            p = PersonFactory.Generate(); // see implementation for another way of returning an instance.

            Assert.IsInstanceOf<Person>(p);
        }

        [Test]
        public void PartialMethods()
        {
            Foo f = new();
            f.Run();
        }
    }


    public partial class Foo
    {
        partial void Bar(string s) => Console.WriteLine(s);
        void Zip(string s) => Console.WriteLine(s);
    }

    public partial class Foo
    {
        partial void Bar(string s);

        public void Run()
        {
            Bar("Here we go");
            Zip("flibble");
        }

        /// <summary>
        /// called by the runtime before any other field access or method invocation within the entire module
        /// </summary>
        [ModuleInitializer]
        public static void ThisRunsFirst()
        {
            Console.WriteLine("This runs before anything");
        }
    }
}