using System;
using System.Runtime.CompilerServices;

namespace MJE.CSHarpGrok.Tests.Unit.V9
{
    public class Unused
    {
        [ModuleInitializer]
        public static void ThisRunsFirst()
        {
            Console.WriteLine("This runs before anything - even from an unused type");
        }
    }
}