﻿global using MJE.CSHarpGrok.Tests.Unit.V10.Additional;
using NUnit.Framework;


namespace MJE.CSHarpGrok.Tests.Unit.V10
{
    public class Features
    {
        [Test]
        public void TestOfNewReadOnlyTypes()
        {

            var prs = new PersonReadonlyStruct("dave", "smith");
            var prs2 = new PersonReadonlyStruct("dave", "smith");
            
            Assert.AreEqual(prs,prs2);
            
            var prc = new PersonRecordClass("dave", "smith");
            var prc2 = new PersonRecordClass("dave", "smith");
            
            Assert.AreEqual(prc,prc2);
            
            var prrs = new PersonReadonlyStruct("sally", "smith");
            var prrs2 = new PersonReadonlyStruct("sally", "smith");

            Assert.AreEqual(prrs, prrs2);
            
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public record PersonRecord(string Firstname, string Surname);

    /// <summary>
    ///  Demotes the record is a reference type
    /// </summary>
    public record class PersonRecordClass(string Firstname, string Surname);


    /// <summary>
    ///  Demotes the record is a value type
    /// </summary>
    public record struct PersonStruct(string Firstname, string Surname);
    
    public readonly record struct PersonReadonlyStruct(string Firstname, string Surname);
}

