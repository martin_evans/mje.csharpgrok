﻿// ReSharper disable once CheckNamespace
namespace MJE.CSHarpGrok.Tests.Unit.V10.Additional;

public record TestRecord(int Id, string Description);