﻿using NUnit.Framework;

namespace MJE.CSHarpGrok.Tests.Unit.V10;

public class FeatureTwo
{
    [Test]
    public void UsingGlobalDirectives()
    {
        // TesRecord is in the MJE.CSHarpGrok.Tests.Unit.V10.Additional NS which is defined globally in the other feature file
        var tr = new TestRecord(1, "sku_one");
        Assert.NotNull(tr);
    }
}