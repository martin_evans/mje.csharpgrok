using System;
using NUnit.Framework;

namespace MJE.CSHarpGrok.Tests.Unit.V71
{
    [TestFixture]
    public class Features
    {

        [Test]
        public void DefaultLiteralExpressions()
        {
            Console.WriteLine(default(string));
            Console.WriteLine(default(int));
            Console.WriteLine(default(Boolean));
        }

        [Test]
        public void InferredTupleElementNames()
        {
            var count = 5;
            var label = "Number of items";
            
            // field names of tuple inferred from the names of the variables
            var ret = (label, count);
            
            PassToMethod(ret);
        }
        
        private void PassToMethod((string label, int count) ret)
        {
            Assert.NotNull(ret);
            Console.WriteLine($"{ret.label} : {ret.count}");
        }
    }
}