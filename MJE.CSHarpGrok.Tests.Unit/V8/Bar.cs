using System;

namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    public class Bar : IDisposable
    {

        public void Process()
        {
            Console.WriteLine($"Running {nameof(Process)}");
        }
        
        public void Dispose()
        {
            Console.WriteLine($"Disposing {GetType().Name}");
        }
    }
}