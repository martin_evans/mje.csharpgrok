using System;

namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    public struct V8FooStruct
    {
        public string Name { get; set; }
        
        public string Post { get; set; }

        public V8FooStruct(string name = "Fred")
        {
            Post = "default post";
            Name = name;
        }

        
        /// <summary>
        /// Demonstrates positional patterns for returning values 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="post"></param>
        public void Deconstruct(out string name, out string post)
        {
            (name, post) = (Name, Post);
        }

        /// <summary>
        /// Readonly modifiers helps you signal the intent of the coed while helping the
        /// compiler make optimizations based on that intent.
        /// </summary>
        public readonly void Greet()
        {
            Console.WriteLine($"Hello {Name}");

            // Line below wil not compile as method marked readonly 
            // Name += Name;
        }

    }
}