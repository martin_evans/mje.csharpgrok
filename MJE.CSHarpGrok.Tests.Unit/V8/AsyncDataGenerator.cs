
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    public  class AsyncDataGenerator : IAsyncDisposable 
    {
        /// <summary>
        /// IAsyncEnumerable used to return the values in an asynchronous way
        /// </summary>
        /// <returns></returns>
        public  async IAsyncEnumerable<string> GenerateSequenceOfStrings()
        {
            {
                for (var i = 0; i < 5; i++)
                {
                    await Task.Delay(100);
                    yield return $"value {i}";
                }
            }
        }

        public ValueTask DisposeAsync()
        {
            Console.WriteLine($"{GetType().Name} is being asynchronously disposed");

            return new ValueTask();
        }
    }
}