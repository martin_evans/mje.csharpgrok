using System;

namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    public interface IFoo
    {
        string Bar { get; set; }

        void Process(string arg1);


        /// <summary>
        /// default impl set here
        /// </summary>
        /// <param name="arg1"></param>
        void Process2(string arg1)
        {
            Console.WriteLine(
                $"Default implementation of {nameof(Process2)} method ran from {GetType().Name} with arg1 of {arg1}");
        }
    }
}