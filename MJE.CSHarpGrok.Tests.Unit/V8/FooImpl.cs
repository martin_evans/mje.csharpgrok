using System;

namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    /// <summary>
    /// Implementation of IFoo.Process2 delegated to default method on interface definition 
    /// </summary>
    public class FooImpl : IFoo
    {
        public string Bar { get; set; }

        public void Process(string arg1)
        {
            Console.WriteLine($"{nameof(Process)} method ran from {GetType().Name} with arg1 of {arg1}");
        }
    }
}