using System;
using System.Threading.Tasks;
using NUnit.Framework;

namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    [TestFixture]
    public class Features
    {
        [Test]
        public void ReadonlyMembers()
        {
            var s = new V8FooStruct("dave");

            s.Greet();

            s.Name = "sally";

            s.Greet();
        }

        [Test]
        public void DefaultInterfaceMethods()
        {
            IFoo foo = new FooImpl() {Bar = "fred"};

            foo.Process("fred");

            foo.Process2("sally");


            foo = new FooImpl2() {Bar = "Dave"};

            foo.Process("fred");

            foo.Process2("stevie");
        }

        [Test]
        public void SwitchExpressions()
        {
            Func<DayOfWeek, string> getMessageBasedOnDayOfWeek = dayOfWeek =>
                dayOfWeek switch
                {
                    DayOfWeek.Sunday => "dinner",
                    DayOfWeek.Monday => "Hooray it's monday",
                    DayOfWeek.Tuesday => "Blah",
                    DayOfWeek.Wednesday => "Hump day",
                    DayOfWeek.Thursday => "Thirsty Thursday",
                    DayOfWeek.Friday => "Funky Friday",
                    DayOfWeek.Saturday => "Tip run Saturday",
                    _ => "Not a day"
                };

            foreach (var value in Enum.GetValues(typeof(DayOfWeek)))
            {
                Console.WriteLine(getMessageBasedOnDayOfWeek((DayOfWeek) value));
            }
        }

        [Test]
        public void PropertyPatternMatching()
        {
            var obj = new V8FooStruct(name: "Sally"){ Post = "Test"};

            static string GenerateMessageBasedOnNameProperty(V8FooStruct s) =>
                s switch
                {
                    { Name: "Sally", Post: "Test"} => $"your name is {s.Name}",
                    { Name: "Sally" } => $"your name is {s.Name} but post is not test",
                    { Name: "Fred" } => $"Default name is {s.Name}",
                    _ => "unknown name"
                };

            var msg = GenerateMessageBasedOnNameProperty(obj);

            Console.WriteLine(msg);
        }

        [Test]
        public void TuplePatternMatching()
        {
            var Name = "Martin";
            var Age  = 46;

            string ProcessValues(string name, int age) =>
                (Name: name, Age: age) switch
                {
                    ("Martin", 46) => "Developer",
                    ("Martin", 45) => "Developer last week",
                    _ => "Someone else"
                };

            Console.WriteLine(ProcessValues(Name, Age));

            Console.WriteLine(ProcessValues(name: "fred", age: 35));
        }

        [Test]
        public void PositionalPatterns()
        {
            var f = new V8FooStruct("martin") {Post = "dev"};

            f.Deconstruct(out var name, out var post);

            Console.WriteLine($" name : {name}; post : {post}");
        }

        [Test]
        public void UsingDeclarations()
        {
            // No need to create an enclosing scope any more
            using var b = new Bar();

            b.Process();
        }

        [Test]
        public void StaticLocalFunctions()
        {
            var s = "fred";

            string ProcessString(string initialValue) => $"string value was {initialValue}  + {s}";

            static string ProcessStringStatically(string initialValue) =>
                $"string value was {initialValue}  + {"swap this for s like above and it wont compile"}";

            ProcessString(s);
            ProcessStringStatically(s);
        }

        [Test]
        public void DisposableReferenceStruct()
        {
            using var st = new DisposableStruct("FRED");

            Console.WriteLine($"Struct for {st.Name} is in scope - check for disposal message at the end of this test");
        }


        /// <summary>
        /// The annotations define a nullable annotation context
        ///  This instructs the compiler to raise warnings accordingly when
        /// the intent of the code is violated by assigning a variable to a null value
        /// </summary>
        [Test]
        public void NullableReferenceTypes()
        {
#nullable enable annotations
            static void Handle(Bar bar) => bar?.Process();

            Bar? s = new Bar();
            Assert.NotNull(s);

            Bar? f = null;

            Handle(s);
            Handle(f);
#nullable disable annotations
        }


        [Test]
        public async Task AsynchronousStreams()
        {
            var generator = new AsyncDataGenerator();

            // use "await foreach" to enumerate sequence generated asynchronously
            await foreach (var val in generator.GenerateSequenceOfStrings())
            {
                Console.WriteLine(val);
            }
        }

        [Test]
        public async Task AsynchronousDisposable()
        {
            // use the "await using" declaration as it implements IAsyncDisposable
            await using var generator = new AsyncDataGenerator();
        }

        [Test]
        public void IndicesAndRanges()
        {
            var words = new[] {"foo", "bar", "smash", "car", "fink", "wow"};

            // The circumflex operator (^) specifies an index relative to the end of the sequence
            Console.WriteLine(words[^3]);

            Console.WriteLine("\nRange:");
            var range = words[2..5];

            foreach (var s in range)
            {
                Console.WriteLine(s);
            }


            Console.WriteLine("\nRange related to end of sequence:");

            range = words[2..^2];

            foreach (var s in range)
            {
                Console.WriteLine(s);
            }


            Console.WriteLine("\nRange related to predefined range variable:");

            var rangeAsVariable = 3..;

            foreach (var word in words[rangeAsVariable])
            {
                Console.WriteLine(word);
            }

        }

        [Test]
        public void NullCoalescingAssignment()
        {
            static string Process(string val)
            {
                // only set val to fred if its null
                val ??= "fred";
                return val;
            }

            string s = null;

            var val = Process(s);

            Assert.AreEqual("fred", val);

            s = "sally";

            val = Process(s);

            Assert.AreNotEqual("fred", val);

            Console.WriteLine(val);
        }


        [Test]
        public void UnManagedTypes()
        {
            Console.WriteLine(sizeof(bool));
            Console.WriteLine(sizeof(short));
            Console.WriteLine(sizeof(int));
            Console.WriteLine(sizeof(long));
            Console.WriteLine(sizeof(DayOfWeek));


            // AS this struct only contains unmanaged types (ints in this case) it can
            // be treated as an unmanaged type
            var u = new UnmanagedStruct<int>() {X = 1, Y = 3};
        }
    }
}