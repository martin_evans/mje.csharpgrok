using System;

namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    public ref struct DisposableStruct
    {

        public string Name { get; set; }

        public DisposableStruct(string name) => Name = name;
        
        /// <summary>
        /// A
        /// </summary>
        public void Dispose()
        {
            Console.WriteLine($"{Name} is disposing");
        }
        
    }
}