namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    public struct UnmanagedStruct<T>
    {
        public T X { get; set; }
        public T Y { get; set; }
    }
}