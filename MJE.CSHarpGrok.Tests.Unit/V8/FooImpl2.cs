using System;

namespace MJE.CSHarpGrok.Tests.Unit.V8
{
    /// <summary>
    /// Implementation of IFoo.Process2 overridden in the class 
    /// </summary>
    public class FooImpl2 : IFoo
    {
        public string Bar { get; set; }

        public void Process(string arg1)
        {
            Console.WriteLine($"{nameof(Process)} method ran from {GetType().Name} with arg1 of {arg1}");
        }

        public void Process2(string arg1)
        {
            Console.WriteLine(
                $"Overridden implementation {nameof(Process2)} method ran from {GetType().Name} with arg1 of {arg1}");
        }
    }
}