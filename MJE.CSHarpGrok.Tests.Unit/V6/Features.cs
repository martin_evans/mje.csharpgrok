using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using NUnit.Framework;

namespace MJE.CSHarpGrok.Tests.Unit.V6
{
    [TestFixture]
    public class Features
    {
        [Test]
        public void ReadonlyProperties()
        {
            var s = new Foo();
            Assert.AreEqual(nameof(Foo), s.Bar);
        }

        [Test]
        public void AutoPropertyInitializers()
        {
            var s = new Foo();
            Assert.IsInstanceOf<List<string>>(s.Vals);
            Assert.NotNull(s.Vals);
        }

        [Test]
        public void ExpressionBodiedFunctionMembers()
        {
            var s = new Foo();
            Assert.IsNotEmpty(s.ToString());
            TestContext.Out.WriteLine(s.ToString());
        }

        [Test]
        public void StaticImportStatements()
        {
            var s = new Foo();
            Assert.GreaterOrEqual(s.ValsCalculatedCount(), 0);
        }

        [Test]
        public void NullConditionalOperators()
        {
            Foo s = null;
            
            Assert.IsNull(s?.Forename);

            s = new Foo() { Forename = "fred" };
            
            Assert.NotNull(s?.Forename);
            
        }

        [Test]
        public void StringInterpolation()
        {
           var s = new Foo() { Forename = "fred", Surname="Smith" };
            
           Assert.IsNotNull(s.FullName);
        }
        
        [Test]
        public void ExceptionFilters()
        {
            var s = new Foo();

            try
            {
                s.BlowUp(code: "flibble");
            }
            catch (Exception e) when (e.Message.Contains("flibble"))
            {
                Assert.Pass();
            }
            catch (Exception e) when (e.Message.Contains("fl0bble"))
            {
                Assert.Pass();
            }
            
        }

        [Test]
        public void InitialiseAssociatedCollectionsUsingIndexers()
        {
            var d = new Dictionary<int, string>()
            {
                [100] = "Foo",
                [200] = "Bar",
                [300] = "FooBar",
            };
            
            d.Add(400, "fred");
            
            Assert.AreEqual(d[300], "FooBar");
            
        }
        
        
        [Test]
        public void InitialiseNameValueCollectionsUsingIndexers()
        {
            var d = new NameValueCollection()
            {
                ["100"] = "Foo",
                ["200"] = "Bar",
                ["300"] = "FooBar",
            };
            
            d.Add("400", "fred");
            
            Assert.AreEqual(d["300"], "FooBar");
            
        }
        
    }
}