using System;
using System.Collections.Generic;
using static System.Math;

namespace MJE.CSHarpGrok.Tests.Unit.V6
{
    public class Foo
    {
        
        /// <summary>
        /// ReadonlyProperties
        /// </summary>
        public  string Bar { get; }
        
        /// <summary>
        /// AutoPropertyInitializers
        /// </summary>
        public List<string> Vals { get; } = new List<string>();

        public string Forename { get; set; }
        public string Surname { get; set; }
        
        /// <summary>
        /// Example of string interpolation
        /// </summary>
        public string FullName => $"{Forename} {Surname}";

        
        /// <summary>
        /// Demonstrates use of nameof expression
        /// </summary>
        public Foo()
        {
            Bar = nameof(Foo);
        }

        /// <summary>
        /// Demonstrates use of ExpressionBodiedFunctionMembers
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"{Bar} - Vals Length - {Vals.Count}";
        
        /// <summary>
        /// Demonstrates use of static import of System.Math 
        /// </summary>
        /// <returns></returns>
        public double ValsCalculatedCount()
        {
            return Pow(Vals.Count, 2);
        }
        
        public void BlowUp(string code)
        {
            throw new Exception($"Exception caused by {code}");
        }
    }
}